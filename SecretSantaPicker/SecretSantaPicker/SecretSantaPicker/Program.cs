﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SecretSantaPicker
{
    class Program
    {
        static void Main(string[] args)
        {
            const int randomScaler = 10;
            int permutations = 100;
            var nameList = new Dictionary<int, string>();
            nameList.Add(1, "emma");
            nameList.Add(2, "john");
            nameList.Add(3, "dylan");
            nameList.Add(4, "tjler");
            nameList.Add(5, "andrew");
            nameList.Add(6, "marie");
            nameList.Add(7, "orion");
            nameList.Add(8, "ethan");
            /* remove if you want to type in names and comment the hard-coded names
            System.Console.WriteLine("Enter names:");
            var name = "";
            var nameNumber = 1;
            while (name != "done")
            {
                name = System.Console.ReadLine();
                if (name == "done")
                {
                    if(nameList.Count % 2 != 0)
                    {
                        System.Console.WriteLine("Please enter an even number of people.");
                        name = "";
                    }
                    else
                    {
                        break;
                    }
                }
                else if(name != "")
                {
                    nameList.Add(nameNumber, name);
                    nameNumber++;
                }
            }
            */

            var end = new List<KeyValuePair<string, string>>();
            // go through 'permutations' permutations
            while ((permutations--) > 0)
            {
                var matchedList = new List<KeyValuePair<string, string>>();
                var matchedKeyList = new List<int>();
                var permutationNameList = new Dictionary<int, string>();
                foreach (var n in nameList)
                {
                    permutationNameList.Add(n.Key, n.Value);
                }
                while ((permutationNameList.Count != matchedKeyList.Count / 2) &&
                    (permutationNameList.Count > 0))
                {
                    var tempNameList = new List<KeyValuePair<int, string>>();
                    // deep copy 
                    foreach (var n in permutationNameList)
                    {
                        tempNameList.Add(n);
                    }

                    // remove names already used
                    foreach (var n in tempNameList)
                    {
                        if (matchedKeyList.Contains(n.Key))
                        {
                            permutationNameList.Remove(n.Key);
                        }
                    }

                    // match names
                    foreach (var n in permutationNameList)
                    {
                        var matched = new Random();
                        var matchedNumber = (int)(matched.NextDouble() * randomScaler);
                        if (!matchedKeyList.Contains(matchedNumber) &&
                            !matchedKeyList.Contains(n.Key) &&
                            (matchedNumber != n.Key))
                        {
                            var matchedName = "";
                            if (permutationNameList.TryGetValue(matchedNumber, out matchedName) &&
                                !(matchedList.Contains(new KeyValuePair<string, string>(n.Value, matchedName)) ||
                                 matchedList.Contains(new KeyValuePair<string, string>(matchedName, n.Value))))
                            {
                                if (n.Value != matchedName)
                                {
                                    matchedList.Add(new KeyValuePair<string, string>(n.Value, matchedName));
                                    matchedKeyList.Add(matchedNumber);
                                    matchedKeyList.Add(n.Key);
                                }
                            }
                        }
                    }
                }
                end.AddRange(matchedList);
                /*
                foreach (var n in matchedList)
                {
                    System.Console.WriteLine(String.Format("{0} - {1}", n.Key, n.Value));
                }
                */
            }
            
            var group = end.GroupBy(i => i);
            var finalCount = new Dictionary<string, int>();
            foreach (var grp in group)
            {
                Console.WriteLine("{0} {1}", grp.Key, grp.Count());
                finalCount.Add(grp.Key.ToString(), grp.Count());
            }
            var results = finalCount.GroupBy(i => i.Key);
            Thread.Sleep(10000);
        }

        private static bool FindName(ICollection<string> keys, string name)
        {
            if(keys.Contains(name))
            {
                return true;
            }
            return false;
                   
        }
    }

}

